package cl.android.appguia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cl.android.appguia.network.RestClient;
import cl.android.appguia.network.request.Usuarios;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditActivity extends AppCompatActivity {
    private TextView idUsuario, nuevoNombre, nuevoApellido, nuevoNombreUsuario;
    private Button botonGuardar, botonVolver;
    private Usuarios currentUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        this.idUsuario = findViewById(R.id.idUsuario);
        this.nuevoNombre = findViewById(R.id.nuevoNombre);
        this.nuevoApellido = findViewById(R.id.nuevoApellido);
        this.nuevoNombreUsuario = findViewById(R.id.nuevoNombreUsuario);
        this.botonGuardar = findViewById(R.id.botonGuardar);
        this.botonVolver = findViewById(R.id.botonVolver);

        Intent intent = getIntent();
        final String idUsuarioFromIntent = intent.getStringExtra("idUsuario");
        this.idUsuario.setText(idUsuarioFromIntent);
        this.getUsuario(idUsuarioFromIntent);

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentUsuario.setNombre(nuevoNombre.getText().toString());
                currentUsuario.setApellido(nuevoApellido.getText().toString());
                currentUsuario.setNombre_usuario(nuevoNombreUsuario.getText().toString());

                RestClient.postSetup().updateUsuario(currentUsuario.getId(), currentUsuario).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(EditActivity.this, "Usuario editado exitosamente", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(EditActivity.this, "Falla en servicio", Toast.LENGTH_SHORT).show();
                        System.out.println("Error: " + t.getMessage());
                    }
                });
            }
        });

        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

    }

    private void getUsuario(String idUsuario) {
        RestClient.postSetup().getUsuario(idUsuario).enqueue(new Callback<Usuarios>() {
            @Override
            public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                currentUsuario = response.body();
            }

            @Override
            public void onFailure(Call<Usuarios> call, Throwable t) {
                Toast.makeText(EditActivity.this, "Falla en servicio", Toast.LENGTH_SHORT).show();
                back();
            }
        });
    }

    private void back() {
        Intent intent = new Intent(EditActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
