package cl.android.appguia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cl.android.appguia.network.RestClient;
import cl.android.appguia.network.request.Usuarios;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private TextView labelNombre, labelApellido, labelUserName;
    private TextView idUsuario, nombre, apellido, usuario;
    private Button botonConsulta, botonEditar, botonEliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.labelNombre  = findViewById(R.id.nameLabel);
        this.labelApellido  = findViewById(R.id.apellidoLabel);
        this.labelUserName  = findViewById(R.id.usuarioLabel);

        this.idUsuario = findViewById(R.id.idUsuario);
        this.nombre = findViewById(R.id.nombre);
        this.apellido = findViewById(R.id.apellido);
        this.usuario = findViewById(R.id.usuario);
        this.botonConsulta = findViewById(R.id.bConsulta);
        this.botonEditar = findViewById(R.id.bEditar);
        this.botonEliminar = findViewById(R.id.bEliminar);
        this.clean();

        botonConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("El ID ES = " + idUsuario.getText().toString());
                RestClient.postSetup().getUsuario(idUsuario.getText().toString()).enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        if (response.body() != null) {
                            nombre.setText(response.body().getNombre());
                            apellido.setText(response.body().getApellido());
                            usuario.setText(response.body().getNombre_usuario());
                            showData();
                            Toast.makeText(MainActivity.this, "Servicio consumido exitosamente", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "El Usuario no existe", Toast.LENGTH_SHORT).show();
                            clean();
                        }
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        clean();
                        Toast.makeText(MainActivity.this, "Falla en servicio", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        botonEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                intent.putExtra("idUsuario", idUsuario.getText().toString());
                startActivity(intent);
            }
        });

        botonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient.postSetup().deleteUsuario(idUsuario.getText().toString()).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(MainActivity.this, "Usuario eliminado exitosamente", Toast.LENGTH_SHORT).show();
                        clean();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Falla en servicio", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void showData() {
        this.labelNombre.setVisibility(View.VISIBLE);
        this.labelApellido.setVisibility(View.VISIBLE);
        this.labelUserName.setVisibility(View.VISIBLE);
        this.nombre.setVisibility(View.VISIBLE);
        this.apellido.setVisibility(View.VISIBLE);
        this.usuario.setVisibility(View.VISIBLE);
        this.botonEditar.setVisibility(View.VISIBLE);
        this.botonEliminar.setVisibility(View.VISIBLE);
    }

    private void clean() {
        this.nombre.setText("");
        this.apellido.setText("");
        this.usuario.setText("");

        this.botonEditar.setVisibility(View.INVISIBLE);
        this.botonEliminar.setVisibility(View.INVISIBLE);

        this.labelNombre.setVisibility(View.INVISIBLE);
        this.labelApellido.setVisibility(View.INVISIBLE);
        this.labelUserName.setVisibility(View.INVISIBLE);

        this.nombre.setVisibility(View.INVISIBLE);
        this.apellido.setVisibility(View.INVISIBLE);
        this.usuario.setVisibility(View.INVISIBLE);
    }
}
