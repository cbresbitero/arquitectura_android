package cl.android.appguia.network;

import cl.android.appguia.network.request.Usuarios;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface API {
    //https://jsonplaceholder.typicode.com/

    @GET("/usuarios/{id}.json")
    Call<Usuarios> getUsuario(@Path("id") String id);

    @PUT("usuarios/{id}")
    Call<Void> updateUsuario(@Path("id") String id, @Body Usuarios user);

    @DELETE("usuarios/{id}")
    Call<Void> deleteUsuario(@Path("id") String id);
}